package com.rakesh.hometest.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class AccountDataStore {
    private static final Logger logger = LoggerFactory.getLogger(AccountDataStore.class);

    private static Map<Integer, Account> accountsMap = new ConcurrentHashMap<Integer, Account>();

    static {
        accountsMap.put(1, new Account(1, 1000.0));
        accountsMap.put(2, new Account(2, 2000.0));
        accountsMap.put(3, new Account(3, 3000.0));
    }

    private AccountDataStore(){}

    public static void putAccount(Account account){
        accountsMap.put(account.getAccountId(), account);
    }

    public static Account getAccount(Integer accountId){
        return accountsMap.get(accountId);
    }

    public static List<Account> getAccounts(){
        return accountsMap.values().stream().collect(Collectors.toList());
    }
}
