package com.rakesh.hometest.model;

import com.rakesh.hometest.util.Constants;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.StringJoiner;

public class Transfer {

    @NotNull(message = Constants.DEBIT_ACCOUNT_ID_CAN_NOT_BE_NULL)
    private Integer debitAccountId;

    @NotNull(message = Constants.CREDIT_ACCOUNT_ID_CAN_NOT_BE_NULL)
    private Integer creditAccountId;

    @NotNull(message = Constants.AMOUNT_CAN_NOT_BE_NULL)
    private Double amount;

    public Transfer() {
    }

    public Transfer(Integer debitAccountId, Integer creditAccountId, Double amount) {
        this.debitAccountId = debitAccountId;
        this.creditAccountId = creditAccountId;
        this.amount = amount;
    }

    public Integer getDebitAccountId() {
        return debitAccountId;
    }

    public void setDebitAccountId(Integer debitAccountId) {
        this.debitAccountId = debitAccountId;
    }

    public Integer getCreditAccountId() {
        return creditAccountId;
    }

    public void setCreditAccountId(Integer creditAccountId) {
        this.creditAccountId = creditAccountId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transfer)) return false;
        Transfer transfer = (Transfer) o;
        return Objects.equals(getDebitAccountId(), transfer.getDebitAccountId()) &&
                Objects.equals(getCreditAccountId(), transfer.getCreditAccountId()) &&
                Objects.equals(getAmount(), transfer.getAmount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDebitAccountId(), getCreditAccountId(), getAmount());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Transfer.class.getSimpleName() + "[", "]")
                .add("debitAccountId=" + debitAccountId)
                .add("creditAccountId=" + creditAccountId)
                .add("amount=" + amount)
                .toString();
    }
}
