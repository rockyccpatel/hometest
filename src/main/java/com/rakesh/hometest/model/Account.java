package com.rakesh.hometest.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;
import java.util.StringJoiner;

@XmlRootElement
public class Account {

    private Integer accountId;
    private Double balance;

    public Account(Integer accountId, Double balance) {
        this.accountId = accountId;
        this.balance = balance;
    }

    public Account() {
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public void creditBalance(Double creditAmount) {
        this.balance += creditAmount;
    }

    public void debitBalance(Double debitAmount) {
        this.balance -= debitAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        Account account = (Account) o;
        return Objects.equals(getAccountId(), account.getAccountId()) &&
                Objects.equals(getBalance(), account.getBalance());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAccountId(), getBalance());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Account.class.getSimpleName() + "[", "]")
                .add("accountId=" + accountId)
                .add("balance=" + balance)
                .toString();
    }
}