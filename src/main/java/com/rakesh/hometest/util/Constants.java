package com.rakesh.hometest.util;

public class Constants {
    public static final String DEBIT_ACCOUNT_IS_INVALID = "Debit account is INVALID";
    public static final String CREDIT_ACCOUNT_IS_INVALID = "Credit account is INVALID";
    public static final String DEBIT_AND_CREDIT_CAN_NOT_BE_THE_SAME_ACCOUNT = "Debit and Credit can not be the same account.";
    public static final String INSUFFICIENT_BALANCE_IN_THE_DEBIT_ACCOUNT = "Insufficient balance in the Debit account.";
    public static final String TRANSFER_COMPLETED_SUCCESSFULLY = "Transfer completed successfully.";
    public static final String TRANSFER_AMOUNT_SHOULD_BE_GREATER_THAN_0_0 = "Transfer amount should be greater than 0.0";
    public static final String TRANSFER_OBJECT_CAN_NOT_BE_NULL = "transfer object can not be null";
    public static final String AMOUNT_CAN_NOT_BE_NULL = "amount can not be null";
    public static final String CREDIT_ACCOUNT_ID_CAN_NOT_BE_NULL = "creditAccountId can not be null";
    public static final String DEBIT_ACCOUNT_ID_CAN_NOT_BE_NULL = "debitAccountId can not be null";
    public static final String ACCOUNT_ID_CAN_NOT_BE_NULL = "accountId can not be null";
    public static final String ACCOUNT_NOT_FOUND = "Account not found";
}
