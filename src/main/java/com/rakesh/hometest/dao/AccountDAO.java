package com.rakesh.hometest.dao;


import com.rakesh.hometest.model.Account;

import java.util.List;

public interface AccountDAO {

    public List<Account> accounts();

    public Account accountDetails(Integer accountId);

    public void transfer(Account debitAccount, Account creditAccount, Double amount);
}