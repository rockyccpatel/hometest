package com.rakesh.hometest.dao;

import com.rakesh.hometest.model.Account;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.rakesh.hometest.model.AccountDataStore.getAccount;
import static com.rakesh.hometest.model.AccountDataStore.getAccounts;
import static com.rakesh.hometest.model.AccountDataStore.putAccount;

public class AccountDAOImpl implements AccountDAO{

    private ReadWriteLock rwLock = new ReentrantReadWriteLock(true);

    public List<Account> accounts() {
        return getAccounts();
    }

    public Account accountDetails(Integer accountId) {
        return getAccount(accountId);
    }

    public void transfer(Account debitAccount, Account creditAccount, Double amount) {
        try {
            rwLock.writeLock().lock();
            debitAccount.debitBalance(amount);

            creditAccount.creditBalance(amount);

            putAccount(debitAccount);
            putAccount(creditAccount);

        } catch (Exception e) {
            throw e;
        } finally {
            try {
                rwLock.writeLock().unlock();
            } catch (Exception ignore) {
                // Exception is thrown if lock is not locked - Ignore
            }
        }
    }
}