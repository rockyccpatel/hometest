package com.rakesh.hometest;

import com.rakesh.hometest.validation.TransferDataValidator;
import com.rakesh.hometest.validation.ValidationConfigurationContextResolver;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.validation.ValidationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.util.Optional;

/**
 * Main Application class which is run.
 *
 */
public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);
  
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI;
    public static final Optional<String> HOSTNAME;
    public static final String APP_PATH;
    public static final Optional<String> PORT;
    
    static{
      HOSTNAME = Optional.ofNullable(System.getenv("HOSTNAME"));
      PORT = Optional.ofNullable(System.getenv("PORT"));
      APP_PATH = "accountmanager";
      BASE_URI = "http://" + HOSTNAME.orElse("localhost") + ":" + PORT.orElse("8080") + "/" + APP_PATH + "/";
    }
    
    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers in com.rakesh.hometest package
        final ResourceConfig resourceConfig = new ResourceConfig()
                .packages("com.rakesh.hometest")
                .property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true)//For Validation
                .property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true)//For Validation
                .register(TransferDataValidator.class)//For Validation
                .register(ValidationConfigurationContextResolver.class)//For Validation
                .register(ValidationFeature.class)//For Validation
                .register(MoxyJsonFeature.class);//For JSON Support

        // create and start a new instance of grizzly http server exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), resourceConfig);
    }

    /**
     * Application method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();

        logger.info(String.format("Jersey app started with WADL available at " + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.stop();
    }
}