package com.rakesh.hometest.service;


import com.rakesh.hometest.dao.AccountDAO;
import com.rakesh.hometest.dao.AccountDAOImpl;
import com.rakesh.hometest.model.Account;
import com.rakesh.hometest.model.Transfer;
import com.rakesh.hometest.util.Constants;
import com.rakesh.hometest.validation.TransferDataValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("accounts")
public class AccountResource {

    private static final Logger logger = LoggerFactory.getLogger(AccountResource.class);

    private final AccountDAO accountDAO = new AccountDAOImpl();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response accounts() {
        try {
            List<Account> accounts = accountDAO.accounts();

            if (accounts != null) {
                GenericEntity entity = new GenericEntity<List<Account>>(accounts) {};
                return Response.status(Response.Status.OK).entity(entity).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch(Exception e) {
            logger.error("Error occurred", e);
            throw e;
        }
    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{accountId}")
    @NotNull(message= Constants.ACCOUNT_ID_CAN_NOT_BE_NULL)
    public Response account(@PathParam("accountId") Integer accountId) {
        try {
            Account account = accountDAO.accountDetails(accountId);

            if (account != null) {
                return Response.status(Response.Status.OK).entity(account).build();
            } else {
                //return Response.status(Response.Status.NOT_FOUND).build();
                return createResponse(Response.Status.BAD_REQUEST, Constants.ACCOUNT_NOT_FOUND);
            }
        } catch(Exception e) {
            logger.error("Error occurred", e);
            throw e;
        }
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces(MediaType.APPLICATION_JSON)
    @NotNull(message= Constants.TRANSFER_OBJECT_CAN_NOT_BE_NULL)
    @Path("/transfers")
    public Response transfer(@NotNull(message= Constants.TRANSFER_OBJECT_CAN_NOT_BE_NULL)  @Valid @TransferDataValidator final Transfer transfer) {
        try {
            Account debitAccount = accountDAO.accountDetails(transfer.getDebitAccountId());
            if (debitAccount == null) {
                return createResponse(Response.Status.BAD_REQUEST, Constants.DEBIT_ACCOUNT_IS_INVALID);
            }

            Account creditAccount = accountDAO.accountDetails(transfer.getCreditAccountId());
            if (creditAccount == null) {
                return createResponse(Response.Status.BAD_REQUEST, Constants.CREDIT_ACCOUNT_IS_INVALID);
            }

            if (debitAccount.getAccountId() == creditAccount.getAccountId()) {
                return createResponse(Response.Status.BAD_REQUEST, Constants.DEBIT_AND_CREDIT_CAN_NOT_BE_THE_SAME_ACCOUNT);
            }

            if (debitAccount.getBalance() < transfer.getAmount()) {
                return createResponse(Response.Status.BAD_REQUEST, Constants.INSUFFICIENT_BALANCE_IN_THE_DEBIT_ACCOUNT);

            }

            accountDAO.transfer(debitAccount, creditAccount, transfer.getAmount());

            return createResponse(Response.Status.OK, Constants.TRANSFER_COMPLETED_SUCCESSFULLY);
        } catch(Exception e) {
            logger.error("Error occurred", e);
            throw e;
        }
    }

    private Response createResponse(Response.Status status, Object entity) {
        return Response.status(status).entity(entity).build();
    }
}
