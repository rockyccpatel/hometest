package com.rakesh.hometest.service;

import com.rakesh.hometest.Application;
import com.rakesh.hometest.model.Account;
import com.rakesh.hometest.model.Transfer;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.rakesh.hometest.util.Constants.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AccountResourceTest {


    public static final String ACCOUNTS_PATH = "/accounts/";
    public static final String ACCOUNTS_TRANSFERS_PATH = "/accounts/transfers";

    private HttpServer server;
    private WebTarget accountsTarget;
    private WebTarget transfersTarget;

    private static final Logger logger = LoggerFactory.getLogger(AccountResourceTest.class);

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Application.startServer();

        // create the client
        Client c = ClientBuilder.newClient().register(MoxyJsonFeature.class);
        accountsTarget = c.target(Application.BASE_URI);
        transfersTarget = c.target(Application.BASE_URI).path(ACCOUNTS_TRANSFERS_PATH);
    }

    @After
    public void tearDown() throws Exception {
        server.shutdownNow();
    }

    /**
     * Test to see that all the accounts are fetched.
     */
    @Test
    public void getAccounts() {
        Response response = accountsTarget.path(ACCOUNTS_PATH).request().get(Response.class);
        assertEquals(200, response.getStatus());
        assertTrue("3000.0(Account 3 balance) is contained in the response", response.readEntity(String.class).contains("3000.0"));
    }

    /**
     * Test to see that passing an invalid account number fails.
     */
    @Test
    public void getAccountInvalidAccountNumberShouldFail() {
        Response response = accountsTarget.path(ACCOUNTS_PATH + "5").request().get(Response.class);
        assertEquals(400, response.getStatus());
        assertTrue(ACCOUNT_NOT_FOUND, response.readEntity(String.class).contains(ACCOUNT_NOT_FOUND));
    }

    /**
     * Test to see a valid account is fetched.
     */
    @Test
    public void getAccountHappyPath() {
        Response response = accountsTarget.path(ACCOUNTS_PATH + "1").request().get(Response.class);
        assertEquals(200, response.getStatus());

        Account account = response.readEntity(Account.class);
        assertEquals("accountId should be 1", new Integer(1), account.getAccountId());
    }

    /**
     * Test to see that a transfer fails when null Object is passed.
     */
    @Test
    public void transferNullObjectShouldFail() {
        Response response = transfersTarget.request().post(Entity.entity(null, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
    }

    /**
     * Test to see that a transfer fails when null debitAccount is passed.
     */
    @Test
    public void nullDebitAccountShouldFail() {
        //Create a test Transfer with null object
        Transfer testTransfer = new Transfer(null, 2, 10.0);;
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
        assertTrue(DEBIT_ACCOUNT_ID_CAN_NOT_BE_NULL, response.readEntity(String.class).contains(DEBIT_ACCOUNT_ID_CAN_NOT_BE_NULL));
    }

    /**
     * Test to see that a transfer fails when null creditAccount is passed.
     */
    @Test
    public void nullCreditAccountShouldFail() {
        //Create a test Transfer with null object
        Transfer testTransfer = new Transfer(1, null, 10.0);;
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
        assertTrue(CREDIT_ACCOUNT_ID_CAN_NOT_BE_NULL, response.readEntity(String.class).contains(CREDIT_ACCOUNT_ID_CAN_NOT_BE_NULL));
    }

    /**
     * Test to see that a transfer fails when debitAccount and creditAccount are same.
     */
    @Test
    public void transferToSameAccountShouldFail() {
        //Create a test Transfer with null object
        Transfer testTransfer = new Transfer(1, 1, 10.0);;
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
        assertTrue(DEBIT_AND_CREDIT_CAN_NOT_BE_THE_SAME_ACCOUNT, response.readEntity(String.class).contains(DEBIT_AND_CREDIT_CAN_NOT_BE_THE_SAME_ACCOUNT));

    }

    /**
     * Test to see that a transfer fails when null amount to be transfered is passed.
     */
    @Test
    public void nullAmountShouldFail() {
        //Create a test Transfer with null amount
        Transfer testTransfer = new Transfer(1, 2, null);;
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
        assertTrue(AMOUNT_CAN_NOT_BE_NULL, response.readEntity(String.class).contains(AMOUNT_CAN_NOT_BE_NULL));
    }

    /**
     * Test to see that a transfer fails when negative amount to be transfered is passed.
     */
    @Test
    public void negativeAmountShouldFail() {
        //Create a test Transfer with null object
        Transfer testTransfer = new Transfer(1, 2, -1.0);;
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
        assertTrue(TRANSFER_AMOUNT_SHOULD_BE_GREATER_THAN_0_0, response.readEntity(String.class).contains(TRANSFER_AMOUNT_SHOULD_BE_GREATER_THAN_0_0));
    }

    /**
     * Test to see that a transfer fails when negative amount to be transfered is passed.
     */
    @Test
    public void zeroAmountShouldFail() {
        //Create a test Transfer with null object
        Transfer testTransfer = new Transfer(1, 2, 0.0);;
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
        assertTrue(TRANSFER_AMOUNT_SHOULD_BE_GREATER_THAN_0_0, response.readEntity(String.class).contains(TRANSFER_AMOUNT_SHOULD_BE_GREATER_THAN_0_0));
    }

    /**
     * Test to see that a transfer fails when non existing debitAccount is passed.
     */
    @Test
    public void transferFromNonExistingDebitAccountShouldFail() {
        //Create a test Transfer with null object
        Transfer testTransfer = new Transfer(5, 1, 10.0);
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
        assertTrue(DEBIT_ACCOUNT_IS_INVALID, response.readEntity(String.class).contains(DEBIT_ACCOUNT_IS_INVALID));
    }

    /**
     * Test to see that a transfer fails when non existing creditAccount is passed.
     */
    @Test
    public void transferToNonExistingCreditAccountShouldFail() {
        //Create a test Transfer with null object
        Transfer testTransfer = new Transfer(1, 5, 10.0);
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
        assertTrue(CREDIT_ACCOUNT_IS_INVALID, response.readEntity(String.class).contains(CREDIT_ACCOUNT_IS_INVALID));
    }

    /**
     * Test to see that a transfer fails when transfer amount is more than the balance of debitAccount is passed.
     */
    @Test
    public void transferAmountMoreThanBalanceInDebitAccountShouldFail() {
        //Create a test Transfer with null object
        Transfer testTransfer = new Transfer(1, 2, 5000.00);
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 400", 400, response.getStatus());
        assertTrue(INSUFFICIENT_BALANCE_IN_THE_DEBIT_ACCOUNT, response.readEntity(String.class).contains(INSUFFICIENT_BALANCE_IN_THE_DEBIT_ACCOUNT));
    }

    /**
     * Test to see Amount is transferred from one Account to another.
     */
    @Test
    public void transferHappyPath() {
        //Create a test Transfer object. 1 has balance of 1000 and 2 has 2000. After transfer 1 should be 500 and 2 should be 2500
        Transfer testTransfer = new Transfer(1, 2, 500.0);
        Response response = transfersTarget.request().post(Entity.entity(testTransfer, MediaType.APPLICATION_JSON));
        assertEquals("Should return status 200", 200, response.getStatus());
        assertTrue(TRANSFER_COMPLETED_SUCCESSFULLY, response.readEntity(String.class).contains(TRANSFER_COMPLETED_SUCCESSFULLY));

        //Verify the data
        response = accountsTarget.path(ACCOUNTS_PATH + testTransfer.getDebitAccountId()).request().get(Response.class);
        Account debitAccount = response.readEntity(Account.class);
        assertEquals("Balance in debitAccount should be 500", (Double)500.00, debitAccount.getBalance());

        response = accountsTarget.path(ACCOUNTS_PATH + testTransfer.getCreditAccountId()).request().get(Response.class);
        Account creditAccount = response.readEntity(Account.class);
        assertEquals("Balance in creditAccount should be 2500", (Double)2500.00, creditAccount.getBalance());
    }
}
