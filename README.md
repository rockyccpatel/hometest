20 September 2019

*Design Document*

Funds Transfer Service

Author

Rakesh Patel

Table of contents

[Table of contents 2](#_Toc19875729)

[1. About This Document 3](#_Toc19875730)

[2. Requirements 3](#_Toc19875731)

[3. Application Overview: 3](#_Toc19875732)

[3.1. Technology stack 3](#_Toc19875733)

[3.2. Assumptions/Limitations: 4](#assumptionslimitations)

[3.3. Architecture diagram 4](#_Toc19875735)

[3.4. Key Business process 4](#_Toc19875736)

[4. Model 5](#_Toc19875737)

[5. Using the application 5](#_Toc19875738)

[5.1. Building the Application 5](#_Toc19875739)

[5.2. Testing the Application 6](#_Toc19875740)

[5.3. Running the application 6](#_Toc19875741)

[5.4. Endpoints 6](#_Toc19875742)

[5.4.1. GET endpoints 6](#_Toc19875743)

[5.4.2. POST endpoints 6](#_Toc19875744)

[5.5. JSON Schema 7](#_Toc19875745)

[5.5.1. Account Object JSON schema 7](#_Toc19875746)

[5.5.2. Transfer Object JSON schema 7](#_Toc19875747)

1.  About This Document

Document of Understanding encompasses the Service description, Architectural
design, the process flows and other technical details for the proposed Solution.
This also includes information received as requirement and assumptions to
simplify the implementation for the purpose of this test.

1.  Requirements

Design and implement a RESTful API (including data model and the backing
implementation) for

money transfers between accounts.

Explicit requirements:

1. You can use Java or Kotlin.

2. Keep it simple and to the point (e.g. no need to implement any
authentication).

3. Assume the API is invoked by multiple systems and services on behalf of end
users.

4. You can use frameworks/libraries if you like ( except Spring ), but don't
forget about

requirement \#2 and keep it simple and avoid heavy frameworks.

5. The datastore should run in-memory for the sake of this test.

6. The final result should be executable as a standalone program (should not
require a

pre-installed container/server).

7. Demonstrate with tests that the API works as expected.

Implicit requirements:

1. The code produced by you is expected to be of high quality.

2. There are no detailed requirements, use common sense.

Please put your work on github or bitbucket.

1.  Application Overview:

Fund Transfer Service is a restful service which receives requests from various
clients for the money transfer. The Accounts are maintained by the service
in-memory with the balances. A transfer request results in reduction of balance
from the “From/Debit Account” and increase in the balance of the “To/Credit
Account” by the transfer amount requested. The Service receives request in JSON
format which is transformed into domain object to be processed for servicing the
request. The Service validates the request against the business rules and throws
appropriate business exceptions for any violations.

1.  Technology stack

-   Java 8

-   Jersey

-   Junit

-   Slf4j

-   Maven

    1.  Assumptions/Limitations:

1.  The Transaction currency can be ignored

2.  Minimum balance is not required

3.  Gateway/Security/ Acknowledgement is out of scope for the purpose of this
    solution

4.  Message format has been restricted to JSON for simplistic implementation

5.  The datastore is in-memory

6.  Transaction management is out of scope

    1.  Architecture diagram

![](Architecture.png)

The Fund Transfer Service is simple Restful Service which accepts requests from
various clients and transforms, validates and persists the data in a
transactional context thus guaranteeing a consistent state. The Service provides
security and logging throughout the processing of the request. For the purpose
of this Solution Transaction and Security implementation is out of scope.

1.  Key Business process

**Transfer Funds Flow:** In this flow the Client sends a JSON message with the
debit account, credit account and the amount to be transferred.  The basic
validation is done to verify that the accounts exist, debit account has enough
balance and amount to be transferred is more than zero. If the checks pass debit
account is debited and credit account is credited with the amount to be
transferred. 

![](TransferSequence.png)

The detailed Flow is as below (Some steps are not implemented in the solution
for simplicity as per specification)

1. Client Sends JSON data to the exposed endpoint

2. The External Firewall intercepts the message for security and virus checks 
 -- Assumption

3. The Gateway authenticates the message and forwards it to the AccountResource
-- Assumption

4. The AccountService transforms the message and calls the DAO

5. The DAO validates the data to validate the data (Null check and Amount \> 0)

6. The Service gets the Debit Account from DAO and checks if it is Valid

7. Next it gets the Credit Account from DAO and checks if it is valid.

8. It then validates if there is sufficient balance

9. The service then debits the Debit Account with the amount to be transferred

10. Finally it credits the Credit Account with the same amount and the
transaction is committed. (Transaction ignored) 

1.  Model

2.  Using the application

    1.  Building the Application

mvn clean package

1.  Testing the Application

mvn test

1.  Running the application

Once the build process is complete, maven would create 2 jars. A standard
**AccountManager-1.0.0-SNAPSHOT.jar** and another one with all dependencies
included to facilitate testing

**AccountManager-1.0.0-SNAPSHOT-jar-with-dependencies.jar**

We have 2 ways of running the application from the project root;

\>**mvn exec:java**

OR (from Project root)

\>**java -jar target/AccountManager-1.0.0-SNAPSHOT-jar-with-dependencies.jar**

The path for transfer is <http://localhost:8080/accountmanager/transfer>

1.  Endpoints

    1.  GET endpoints

| **URL**                                                    | **HTTP Response**                                                                                   |
|------------------------------------------------------------|-----------------------------------------------------------------------------------------------------|
| http://localhost:8080/accountmanager/accounts              | 200 with JSON for All Account object (3 in this Application                                         |
| http://localhost:8080/accountmanager/accounts /{accountId} | 200 with JSON for Account matching the accountId parameter 400 with “**Account not found**” message |

2.  POST endpoints

| **URL**                                                   | **Sample input**                                           | **HTTP Response**        |
|-----------------------------------------------------------|------------------------------------------------------------|--------------------------|
| <http://localhost:8080/accountmanager/accounts/transfers> | {"debitAccountId": 2, "creditAccountId":1, "amount":500.0} | 200 with success message |

Sample Curl for testing Successful Transfer(Unix based OS required or for
windows applications like Postman/ModaXterm can be used)

curl --header "Content-Type: application/json" --request POST --data
'{"debitAccountId": 1,"creditAccountId": 2,"amount": 500.0}'
<http://localhost:8080/accountmanager/accounts/transfers>

**To verify**

<http://localhost:8080/accountmanager/accounts/1> should show balance of 500

<http://localhost:8080/accountmanager/accounts/2> should show balance of 2500

Please note : If the test is run again without restarting the application the
above verification will have different values.

1.  JSON Schema

    1.  Account Object JSON schema

{

"\$schema": "http://json-schema.org/draft-04/schema\#",

"title": "Account",

"description": "Bank account",

"type": "object",

"properties": {

"accountId": {

"description": "The unique identifier for the account",

"type": "integer"

},

"balance": {

"description": "Balance in the account",

"type": "number"

}

},

"required": ["accountId", "balance"]

}

1.  Transfer Object JSON schema

{

"\$schema": "http://json-schema.org/draft-04/schema\#",

"title": "Transfer",

"description": "Object used to transfer money between accounts",

"type": "object",

"properties": {

"debitAccountId": {

"description": "The accountId of the account to be debited",

"type": "integer"

},

"creditAccountId": {

"description": "The accountId of the account to be credited",

"type": "integer"

},

"amount": {

"description": "amount to be transferred ",

"type": "number"

}

},

"required": ["debitAccountId", "creditAccountId ", "amount"]

}
